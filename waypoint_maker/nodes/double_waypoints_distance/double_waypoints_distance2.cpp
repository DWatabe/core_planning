#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>

class DoubleWaypointsDistance
{
private:
	ros::NodeHandle nh_, p_nh_;

	ros::Subscriber sub_lane_array_;

	double math_distance(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, geometry_msgs::Pose cur)
	{
		double x1 = pose1.position.x, x2 = pose2.position.x;
		double y1 = pose1.position.y, y2 = pose2.position.y;
		double a = y2 - y1;
		double b = x1 - x2;
		double c = - x1 * y2 + y1 * x2;
		//std::cout << x1 << "," << x2 << "," << y1 << "," << y2 << std::endl;
		//double x0 = current_pose_.pose.position.x, y0 = current_pose_.pose.position.y;
		double x0 = cur.position.x, y0 = cur.position.y;
		double db = sqrt(a * a + b * b);
		if(db == 0)
		{
			return 0;
		}
		return (a * x0 + b * y0 + c) / db;
	}
	void callbackLaneArray(const autoware_msgs::LaneArray &msg)
	{
		if(msg.lanes.size() != 2) {std::cout << "error : not lane count 2" << std::endl; return;}
		autoware_msgs::Lane master_lane = msg.lanes[0], slave_lane = msg.lanes[1];
		/*for(int id=0; id<master_lane.waypoints.size(); id++)
		{
			int master_ind = -1, slave_ind = -1;
			for(int i=0; i<master_lane.waypoints.size()-1; i++)
				if(master_lane.waypoints[i].waypoint_param.id == id) {master_ind = i; break;}
			for(int i=0; i<slave_lane.waypoints.size(); i++)
				if(slave_lane.waypoints[i].waypoint_param.id == id) {slave_ind = i; break;}
			if(master_ind == -1 || slave_ind == -1) continue;

			geometry_msgs::Pose pose1 = master_lane.waypoints[master_ind].pose.pose;
			geometry_msgs::Pose pose2 = master_lane.waypoints[master_ind+1].pose.pose;
			geometry_msgs::Pose cur = slave_lane.waypoints[slave_ind].pose.pose;
			double distance = math_distance(pose1, pose2, cur);
			std::cout << "id:" << id << "  dis:" << distance << std::endl;
		}*/
		//Calculate distance
		for(int id=0; id<slave_lane.waypoints.size(); id++)
		{
			double min_dis=1000000000;
			int min_id;
			for(int i=0; i<master_lane.waypoints.size(); i++)
			{
				double x = master_lane.waypoints[i].pose.pose.position.x - slave_lane.waypoints[id].pose.pose.position.x;
				double y = master_lane.waypoints[i].pose.pose.position.y - slave_lane.waypoints[id].pose.pose.position.y;
				double z = master_lane.waypoints[i].pose.pose.position.z - slave_lane.waypoints[id].pose.pose.position.z;
				double dis = sqrt(x*x + y*y + z*z);
				if(dis < min_dis)
				{					
					min_dis = dis;
					min_id = i;
				}
			}

			int route_id1=min_id,route_id2;
			if(route_id1 == 0) route_id2 = route_id1+1;
			else if(route_id1 == master_lane.waypoints.size()-1) route_id2 = route_id1-1;
			else
			{
				double x = master_lane.waypoints[route_id1+1].pose.pose.position.x - slave_lane.waypoints[id].pose.pose.position.x;
				double y = master_lane.waypoints[route_id1+1].pose.pose.position.y - slave_lane.waypoints[id].pose.pose.position.y;
				double z = master_lane.waypoints[route_id1+1].pose.pose.position.z - slave_lane.waypoints[id].pose.pose.position.z;
				double dis_plus = sqrt(x*x + y*y + z*z);
				x = master_lane.waypoints[route_id1-1].pose.pose.position.x - slave_lane.waypoints[id].pose.pose.position.x;
				y = master_lane.waypoints[route_id1-1].pose.pose.position.y - slave_lane.waypoints[id].pose.pose.position.y;
				z = master_lane.waypoints[route_id1-1].pose.pose.position.z - slave_lane.waypoints[id].pose.pose.position.z;
				double dis_minus = sqrt(x*x + y*y + z*z);
				if(dis_plus > dis_minus) route_id2 = route_id1 - 1;
				else route_id2 = route_id1 + 1;
			}
			geometry_msgs::Pose pose1 = master_lane.waypoints[route_id1].pose.pose;
			geometry_msgs::Pose pose2 = master_lane.waypoints[route_id2].pose.pose;
			geometry_msgs::Pose cur = slave_lane.waypoints[id].pose.pose;
			double distance = math_distance(pose1, pose2, cur);
			//std::cout << id << "," << min_dis << std::endl;
			std::cout << id << "," << distance << std::endl;
		}
		//Calculate average of x,y,z
		/*std::vector<int> min_dis_list;
		for(int id=0; id<slave_lane.waypoints.size(); id++)
		{
			double min_dis=1000000000;
			int min_id;
			for(int i=0; i<master_lane.waypoints.size(); i++)
			{
				double x = master_lane.waypoints[i].pose.pose.position.x - slave_lane.waypoints[id].pose.pose.position.x;
				double y = master_lane.waypoints[i].pose.pose.position.y - slave_lane.waypoints[id].pose.pose.position.y;
				double z = master_lane.waypoints[i].pose.pose.position.z - slave_lane.waypoints[id].pose.pose.position.z;
				double dis = sqrt(x*x + y*y + z*z);
				if(dis < min_dis)
				{
					min_dis = dis;
					min_id = i;
				}
			}
			min_dis_list.push_back(min_id);
		}
		
		for(int id=0; id<master_lane.waypoints.size(); id++)
		{
			double x=0, y=0, z=0;
			int count=0;
			for(int i=0; i<slave_lane.waypoints.size(); i++)
			{
				if(min_dis_list[i] == id)
				{
					x+=slave_lane.waypoints[i].pose.pose.position.x;
					y+=slave_lane.waypoints[i].pose.pose.position.y;
					z+=slave_lane.waypoints[i].pose.pose.position.z;
					count++;
				}
			}
			
			if(count != 0)
			{
				x /= count;  y /= count;  z/=count;
			}
			std::cout << "id," << id << ",x," << x << ",y," << y << ",z," << z << std::endl;
		}*/
	}
public:
	DoubleWaypointsDistance(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		sub_lane_array_ = nh_.subscribe("/based/lane_waypoints_raw", 1, &DoubleWaypointsDistance::callbackLaneArray, this);
	}
};
int main(int argc, char** argv)
{
   	ros::init(argc, argv, "double_waypoints_distance");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");
	DoubleWaypointsDistance dwd(nh, private_nh);
	ros::spin();
	return 0;
}
