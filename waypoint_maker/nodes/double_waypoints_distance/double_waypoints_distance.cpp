#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>

class DoubleWaypointsDistance
{
private:
	ros::NodeHandle nh_, p_nh_;

	ros::Subscriber sub_lane_array_;

	double math_distance(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, geometry_msgs::Pose cur)
	{
		double x1 = pose1.position.x, x2 = pose2.position.x;
		double y1 = pose1.position.y, y2 = pose2.position.y;
		double a = y2 - y1;
		double b = x1 - x2;
		double c = - x1 * y2 + y1 * x2;
		//std::cout << x1 << "," << x2 << "," << y1 << "," << y2 << std::endl;
		//double x0 = current_pose_.pose.position.x, y0 = current_pose_.pose.position.y;
		double x0 = cur.position.x, y0 = cur.position.y;
		double db = sqrt(a * a + b * b);
		if(db == 0)
		{
			return 0;
		}
		return (a * x0 + b * y0 + c) / db;
	}

	void callbackLaneArray(const autoware_msgs::LaneArray &msg)
	{
		if(msg.lanes.size() != 2) {std::cout << "error : not lane count 2" << std::endl; return;}
		autoware_msgs::Lane master_lane = msg.lanes[0], slave_lane = msg.lanes[1];

		for(int id=0; id<master_lane.waypoints.size(); id++)
		{
			int master_ind = -1, slave_ind = -1;
			for(int i=0; i<master_lane.waypoints.size()-1; i++)
				if(master_lane.waypoints[i].waypoint_param.id == id) {master_ind = i; break;}
			for(int i=0; i<slave_lane.waypoints.size(); i++)
				if(slave_lane.waypoints[i].waypoint_param.id == id) {slave_ind = i; break;}
			if(master_ind == -1 || slave_ind == -1) continue;

			geometry_msgs::Pose pose1 = master_lane.waypoints[master_ind].pose.pose;
			geometry_msgs::Pose pose2 = master_lane.waypoints[master_ind+1].pose.pose;
			geometry_msgs::Pose cur = slave_lane.waypoints[slave_ind].pose.pose;
			double distance = math_distance(pose1, pose2, cur);
			std::cout << "id:" << id << "  dis:" << distance << std::endl;
		}
	}
public:
	DoubleWaypointsDistance(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		sub_lane_array_ = nh_.subscribe("/based/lane_waypoints_raw", 1, &DoubleWaypointsDistance::callbackLaneArray, this);
	}
};

int main(int argc, char** argv)
{
   	ros::init(argc, argv, "double_waypoints_distance");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	DoubleWaypointsDistance dwd(nh, private_nh);
	ros::spin();
	return 0;
}