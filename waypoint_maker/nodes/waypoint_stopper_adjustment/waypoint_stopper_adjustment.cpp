#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Empty.h>
#include <limits.h>
#include <autoware_config_msgs/ConfigWaypointStopperAdjustment.h>

class SelectPoint
{
public:
	unsigned int id_;
	geometry_msgs::Point point_;
};

class WaypointStopperAdjustment
{
private:
	ros::NodeHandle nh_, p_nh_;

	ros::Publisher pub_point_out_;
	ros::Subscriber sub_global_waypoints_, sub_clicked_point_, sub_start_, sub_config_;

	autoware_config_msgs::ConfigWaypointStopperAdjustment config_;
	autoware_msgs::LaneArray global_waypoints_;
	//std::vector<geometry_msgs::PointStamped> click_point_;
	std::vector<SelectPoint> select_way_points_;

	void callbackConfig(const autoware_config_msgs::ConfigWaypointStopperAdjustment &msg)
	{
		config_ = msg;
	}

	void callbackGlobalWaypoints(const autoware_msgs::LaneArray &msg)
	{
		if(msg.lanes.size() != 1)
			std::cout << "Only read one csv for waypoints" << std::endl;
		else
		{
			global_waypoints_ = msg;
			std::cout << "waypoints read" << std::endl;
		}
	}

	bool waypointCheck()
	{
		if(global_waypoints_.lanes.size() != 1)
		{
			std::cout << "not read waypoints" << std::endl;  return false;
		}
		else if(global_waypoints_.lanes[0].waypoints.size() <= 1)
		{
			std::cout << "lane has no route" << std::endl;  return false;
		}
		return true;
	}

	void callbackClickedPoint(const geometry_msgs::PointStamped &msg)
	{
		if(!waypointCheck())
		{
			return;
		}
		else if(select_way_points_.size() < 2)
		{
			//click_point_.push_back(msg);
			std::cout << "point" << select_way_points_.size() << " push" << std::endl;

			std::vector<autoware_msgs::Waypoint> waypoints = global_waypoints_.lanes[0].waypoints;
			unsigned int min_way_id;
			geometry_msgs::Point min_way_point;
			double min_dt = DBL_MAX;
			for(autoware_msgs::Waypoint way : waypoints)
			{
				double dx = way.pose.pose.position.x - msg.point.x;
				double dy = way.pose.pose.position.y - msg.point.y;
				double dz = way.pose.pose.position.z - msg.point.z;
				double dt = sqrt(dx*dx + dy*dy + dz*dz);
				if(min_dt > dt)
				{
					min_dt = dt;
					min_way_id = way.waypoint_param.id;
					min_way_point = way.pose.pose.position;
				}
			}

			std::cout << "id:" << min_way_id << std::endl;
			std::cout << "x:" << min_way_point.x << " y:" << min_way_point.y << " z:" << min_way_point.z << std::endl;
			SelectPoint sp;
			sp.id_ = min_way_id;  sp.point_ = min_way_point;
			select_way_points_.push_back(sp);
		}
		else std::cout << "There are already two" << std::endl;
	}

	void callbackStart(const std_msgs::Empty &msg)
	{
		if(select_way_points_.size() != 2)
		{
			std::cout << "not 2 select waypoint" << std::endl;
			return;
		} 
		if(select_way_points_[1].id_ - select_way_points_[0].id_ != 1)
		{
			std::cout << "not next id" << std::endl;
			return;
		}

		for(int ind=0; ind<global_waypoints_.lanes[0].waypoints.size(); ind++)
		{
			if(global_waypoints_.lanes[0].waypoints[ind].waypoint_param.id == select_way_points_[1].id_)
			{
				double dx = select_way_points_[1].point_.x - select_way_points_[0].point_.x;
				double dy = select_way_points_[1].point_.y - select_way_points_[0].point_.y;
				double dz = select_way_points_[1].point_.z - select_way_points_[0].point_.z;
				geometry_msgs::Point ret;
				ret.x = global_waypoints_.lanes[0].waypoints[ind].pose.pose.position.x - dx * config_.ratio;
				ret.y = global_waypoints_.lanes[0].waypoints[ind].pose.pose.position.y - dy * config_.ratio;
				ret.z = global_waypoints_.lanes[0].waypoints[ind].pose.pose.position.z - dz * config_.ratio;
				pub_point_out_.publish(ret);
				std::cout << "output" << std::endl;
			}
		}

		select_way_points_.resize(0);
	}
public:
	WaypointStopperAdjustment(ros::NodeHandle nh, ros::NodeHandle p_nh)
		: nh_(nh)
		, p_nh_(p_nh)
	{
		pub_point_out_ = nh_.advertise<geometry_msgs::Point>("/waypoint_stopper_adjustment_output", 1, true);
		sub_global_waypoints_ = nh_.subscribe("/lane_waypoints_array", 1, &WaypointStopperAdjustment::callbackGlobalWaypoints, this);
		sub_clicked_point_ = nh_.subscribe("/clicked_point", 1, &WaypointStopperAdjustment::callbackClickedPoint, this);
		sub_start_ = nh_.subscribe("/waypoint_stopper_adjustment_start", 1, &WaypointStopperAdjustment::callbackStart, this);
		sub_config_ = nh_.subscribe("/config/waypoint_stopper_adjustment", 1, &WaypointStopperAdjustment::callbackConfig, this);
	}
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "waypoint_stopper_adjustment");
	ros::NodeHandle nh;
	ros::NodeHandle private_nh("~");

	WaypointStopperAdjustment wsa(nh, private_nh);

	ros::spin();
    return 0;
}